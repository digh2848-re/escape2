﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndingTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            StartCoroutine("Fall");
            SceneManager.LoadScene("SampleScene");
        }

    }

    IEnumerator Fall()
    {

        yield return new WaitForSecondsRealtime(1f);
        
    }
}
