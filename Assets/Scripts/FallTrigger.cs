﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallTrigger : MonoBehaviour
{
    public GameObject mirrorObject;
    public GameObject player;
    public bool End = true;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            StartCoroutine("Fall");          
        }

    }

    IEnumerator Fall()
    {

        yield return new WaitForSecondsRealtime(1f);
        if (End)
        {
            GameObject.FindWithTag("MainCamera").GetComponent<ExitTrigger>().Ending = false;
            mirrorObject.SetActive(true);
            player.transform.localPosition = new Vector3(-9.874381f, 40.85875f, -17.17606f);
        }
        

    }
}
