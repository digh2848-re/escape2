﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootCtrl : MonoBehaviour
{
    public GameObject playerObject;
    public GameObject CameraDir;
    public GameObject ShootObject;
    public float power;

    private float Timer;
    private float DelayTime = 3f;
    private Vector3 pos;
    public bool Ctrl;

    // Start is called before the first frame update
    void Start()
    {
        power = 100f;
        pos = playerObject.transform.localPosition;
        Ctrl = true;
        Timer = 0;
    }

    public void shoot(Vector3 direction)
    {
        Rigidbody r = ShootObject.GetComponent<Rigidbody>();
        Vector3 speed = direction * power;
        r.isKinematic = false;
        r.useGravity = true;
        r.AddForce(speed);      
    }

    

    // Update is called once per frame
    void Update()
    {
        pos = playerObject.transform.localPosition;

        if (Ctrl && Input.GetMouseButtonDown(0))
        {
            playerObject.GetComponent<FirstPersonController>().cameraCanMove = false;
            playerObject.GetComponent<FirstPersonController>().playerCanMove = false;
            
        }

        if (Ctrl && Input.GetMouseButton(0))
        {

           


            Timer += Time.deltaTime;

            //의자 게이지채우는동안 던지는 힘 증가 및 던지는 동작 수행
            power += 20f;
            
            transform.Translate(Vector3.up*0.01f);
            //지연 2초
            if (Timer > DelayTime)
            {
                Timer = 0;
                Ctrl = false;
                Debug.Log("마우스 게이지 끝");

                ShootObject.SetActive(true);
                pos = pos + Vector3.up*0.5f;
                pos = pos + CameraDir.transform.forward;
                ShootObject.transform.localPosition = pos;
                shoot(CameraDir.transform.forward);
                playerObject.GetComponent<FirstPersonController>().cameraCanMove = true;
                playerObject.GetComponent<FirstPersonController>().playerCanMove = true;
                gameObject.SetActive(false);
            }
        }

        if (Ctrl && Input.GetMouseButtonUp(0))
        {
            ShootObject.SetActive(true);
            pos = pos + Vector3.up * 0.5f;
            pos = pos + CameraDir.transform.forward;
            ShootObject.transform.localPosition = pos;
            shoot(CameraDir.transform.forward);
            playerObject.GetComponent<FirstPersonController>().cameraCanMove = true;
            playerObject.GetComponent<FirstPersonController>().playerCanMove = true;
            gameObject.SetActive(false);
        }
    }
}
