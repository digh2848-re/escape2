﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class enemyscript2 : MonoBehaviour
{
    // 길찾아 이동할 enemy
    NavMeshAgent nav;
    //public Animator anim;


    public GameObject[] targets;

    public GameObject player;
    public GameObject Chair;
    public GameObject Chair2;
    public GameObject Chair3;
    public GameObject ChairItem;
    public GameObject mirrorObject;


    public float PlayerHeight;


    private float heightcheck;
    private int point = 0;
    private bool EnemyStop;
    public bool SafeTiming;
    public bool SafeField;
    // Start is called before the first frame update

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (this.gameObject.tag == "enemy")
            {
                player.transform.localPosition = new Vector3(17.04f, 16.629f, 2.798f);
                ChairItem.SetActive(false);
                Chair.SetActive(true);
                Chair2.SetActive(true);
                Chair3.SetActive(true);
            }
            else if (this.gameObject.tag == "enemy2")
            {
                player.transform.localPosition = new Vector3(16.558f, 32.821f, 3.364f);
                ChairItem.SetActive(false);
                Chair.SetActive(true);
                Chair2.SetActive(true);
                Chair3.SetActive(true);
            }
            else if (this.gameObject.tag == "enemy3")
            {
                //GameObject.FindWithTag("MainCamera").GetComponent<ExitTrigger>().Ending = false;
                mirrorObject.SetActive(true);
                player.transform.localPosition = new Vector3(-9.874381f, 40.85875f, -17.17606f);
                ChairItem.SetActive(false);
                Chair.SetActive(true);
                Chair2.SetActive(true);
                Chair3.SetActive(true);
            }
           

        }

        if (collision.gameObject.tag == "Chair4")
        {
            EnemyStop = true;
            Debug.Log("attack success");
        }
    }

    //순찰 함수
    public void next()
    {
        if (targets.Length == 0) return;

        nav.destination = targets[point].transform.position;
        // 순찰시 속도 및 가속도
        nav.speed = 2f;
        nav.acceleration = 2f;

        point = (point + 1) % targets.Length;

    }

    void Start()
    {
        EnemyStop = false;
        heightcheck = 0f;
        SafeField = true;
        SafeTiming = true;
        // 게임이 시작되면 게임 오브젝트에 부착된 NavMeshAgent 컴포넌트를 가져와서 저장 
        nav = GetComponent<NavMeshAgent>();
        next();
    }

    IEnumerator EnemyStopDelay()
    {
        yield return new WaitForSecondsRealtime(7f);
        EnemyStop = false;

    }

    // Update is called once per frame
    void Update()
    {


        heightcheck = player.transform.localPosition.y;
        //속도가 일정 이하로 떨어질 경우 animation stop
        if(nav.speed <= 0.5)
        {
            //anim.SetBool("isWalk", false);
        }
        else
        {
           // anim.SetBool("isWalk", true);
        }
        //enemy 공격받을때 몇초동안 멈춤
        if (EnemyStop)
        {
            nav.velocity = Vector3.zero;

            StartCoroutine(EnemyStopDelay());

            return;
        }

        
        //9,10,11층 추격
        if (heightcheck > 34.1f && heightcheck < 39.6f)
        {
            nav.destination = player.transform.position;
            //발각후 추적속도
            nav.speed = 3.5f;
            nav.acceleration = 3.5f;
        }
        //발각후 추적(3,4,5,6,7층)
        else if ((heightcheck > PlayerHeight - 0.1f && heightcheck < PlayerHeight + 0.1f) && SafeField)
        {
            nav.destination = player.transform.position;
            //발각후 추적속도
            nav.speed = 3.5f;
            nav.acceleration = 3.5f;

        }
        else //순찰
        {
            //은신처에 숨게되는 타이밍에 맞게 정지후 다시 수색
            if (!SafeTiming)
            {

                nav.speed = 0f;
                nav.acceleration = 0f;
                next();
                SafeTiming = true;
            }

            if (!nav.pathPending && nav.remainingDistance < 2f)
            {

                next();
            }
        }



    }
}

