﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ladderClimb : MonoBehaviour
{
    
    private bool Onladder;
    
    public float speed = 0.1f;
    

    // Start is called before the first frame update
    void Start()
    {       
        Onladder = false;
    }

    void OnTriggerEnter(Collider col)
    {    
            if (col.transform.tag == "Ladder")
            {
                Onladder = true;
                this.GetComponent<FirstPersonController>().playerCanMove = false;
                this.GetComponent<Rigidbody>().useGravity = false;
                this.GetComponent<Rigidbody>().isKinematic = true;

            }
    }

    void OnTriggerExit(Collider Get)
    {        
        Onladder = false;
        this.GetComponent<Rigidbody>().useGravity = true;
        this.GetComponent<Rigidbody>().isKinematic = false;
        this.GetComponent<FirstPersonController>().playerCanMove = true;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {      
        if (Onladder)
        {
            if (Input.GetKey(KeyCode.W))
            {
                this.transform.Translate(0, speed, 0);
            }

            if (Input.GetKey(KeyCode.S))
            {
                this.transform.Translate(0, -speed, 0);
            }
        }      
    }
}
