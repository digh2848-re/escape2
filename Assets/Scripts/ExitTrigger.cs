﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ExitTrigger : MonoBehaviour
{
    public GameObject player;
    public bool Ending = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            StartCoroutine("EndingGame");
        }

    }

    IEnumerator EndingGame()
    {

        yield return new WaitForSecondsRealtime(2f);
        if (Ending)
        {
            SceneManager.LoadScene(0);
        }


    }
}
