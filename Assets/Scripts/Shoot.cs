﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    private float Timer;
    private float DelayTime = 3f;
  

    // Start is called before the first frame update
    void Start()
    {
        Timer = 0;
    }




    // Update is called once per frame
    void Update()
    {

        Timer += Time.deltaTime;

        if (Timer > DelayTime)
        {
            gameObject.SetActive(false);
            Timer = 0;
        }
    }
}
