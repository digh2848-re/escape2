﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class gameManager : MonoBehaviour
{
    private static gameManager instance;

    private float xx;
    private float yy;
    private float zz;
    private Vector3 pos;
    public static gameManager Instance
    {
        get
        {
            if (instance == null)
            {
                var obj = FindObjectOfType<gameManager>();
                if (obj != null)
                {
                    instance = obj;
                }
                else
                {
                    var newObj = new GameObject().AddComponent<gameManager>();
                }
            }
            return instance;
        }
    }

    private void awake()
    {
        var objs = FindObjectsOfType<gameManager>();
        if (objs.Length != 1)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }

    public void Save(float x,float y,float z)
    {
        PlayerPrefs.SetFloat("X", x);
        PlayerPrefs.SetFloat("Y", y);
        PlayerPrefs.SetFloat("Z", z);
    }

    public Vector3 Load()
    {
        xx = PlayerPrefs.GetFloat("X");
        yy = PlayerPrefs.GetFloat("Y");
        zz= PlayerPrefs.GetFloat("Z");

        pos = new Vector3(xx, yy, zz);
        return pos;
    }

}
