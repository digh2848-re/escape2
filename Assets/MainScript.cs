﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class MainScript : MonoBehaviour
{
    public static bool settingopen = false;

    public GameObject settingUI;
    public AudioMixer audioMixer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!settingopen)
            {
                ExitGame();
            }
            else if (settingopen)
            {
                settingClose();
            }
        }
    }

    public void startScene()
    {
        SceneManager.LoadScene(1);
    }


    public void setting()
    {
        settingUI.SetActive(true);
        settingopen = true;
    }

    public void ExitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit(); // 어플리케이션 종료
#endif
    }


    public void SetBGVolume(float BGVolume)
    {
        audioMixer.SetFloat("BGVolume", BGVolume);
    }

    public void SetSFXVolume(float SFXVolume)
    {
        audioMixer.SetFloat("SFXVoulme", SFXVolume);
    }

    public void SetFullscreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
    }

    public void settingClose()
    {
        settingUI.SetActive(false);
        settingopen = false;
    }
}
